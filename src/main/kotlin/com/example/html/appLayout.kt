package com.example.html

import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.resources.*
import kotlinx.html.*

class LayoutTemplate(private  val application: Application) : Template<HTML> {
    val content = Placeholder<FlowContent>()

    override fun HTML.apply() {
        head {
        }
        body {
            nav() {
                ul() {
                    li() {
                        a() {
//                            href = call.url("/users") // Genera una URL relativa a "/users"
//                            +"Users"
                        }
                    }
                    li() {
                        a() {
                            //href = application.href("")
                        }
                    }
                }
            }
            main() {
                insert(content)
            }
        }
    }
}