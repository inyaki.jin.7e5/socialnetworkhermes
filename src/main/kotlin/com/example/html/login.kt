package com.example.html

import com.example.routes.Posts
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.server.application.*
import kotlinx.html.*
import kotlinx.html.stream.createHTML
import kotlinx.html.stream.appendHTML

class LoginFormTemplate(private val application: ContentType.Application) : Template<HTML> {
    val content = Placeholder<FlowContent>()

    override fun HTML.apply() {
        head {
            title { +"Login" }
            style {
                unsafe {
                    raw("""
                        body {
                            font-family: Arial, sans-serif;
                            margin: 0;
                            padding: 0;
                        }
                        .container {
                            display: flex;
                            justify-content: center;
                            align-items: center;
                            height: 100vh;
                        }
                        .login-form {
                            max-width: 300px;
                            padding: 16px;
                            border: 1px solid #ccc;
                            border-radius: 4px;
                            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                            background-color: #fff;
                        }
                        .form-group {
                            margin-bottom: 16px;
                        }
                        .form-label {
                            display: block;
                            margin-bottom: 8px;
                            font-weight: bold;
                        }
                        .form-control {
                            width: 100%;
                            padding: 8px;
                            font-size: 14px;
                            border: 1px solid #ccc;
                            border-radius: 4px;
                        }
                        .form-button {
                            width: 100%;
                            padding: 8px;
                            font-size: 14px;
                            border: none;
                            border-radius: 4px;
                            background-color: #4CAF50;
                            color: #fff;
                            cursor: pointer;
                        }
                        .form-button:hover {
                            background-color: #45a049;
                        }
                        .recent-updates {
                            margin-top: 16px;
                        }
                        .new-post {
                            margin-top: 8px;
                            font-size: 12px;
                            color: #888;
                        }
                    """.trimIndent())
                }
            }
        }
        body {
            div(classes = "container") {
                div(classes = "login-form") {
                    h2 { +"Login" }
                    form {
                        div(classes = "form-group") {
                            label(classes = "form-label") { +"Username" }
                            input(classes = "form-control", type = InputType.text) { }
                        }
                        div(classes = "form-group") {
                            label(classes = "form-label") { +"Password" }
                            input(classes = "form-control", type = InputType.password) { }
                        }
                        div(classes = "form-group") {
                            input(classes = "form-button", type = InputType.submit) { value = "Login" }
                        }
                    }
                }
                div(classes = "recent-updates") {
                    h6 { +"Recent updates" }
                    small(classes = "new-post") {
                        a {
                            href = application.href(Posts.New())
                            +"New Post"
                        }
                    }
                }
            }
        }
    }
}
