package com.example.html.users

import kotlinx.html.*
import com.example.models.UserStats

fun FlowContent.platformStatsView(platformStats: List<UserStats>) {
    div() {
        h6() { +"""Platforms""" }
        ul {
            platformStats.forEach { user ->
                li {
                    +"${user.user.username} ${user.postCount}"
                }
            }
        }
    }
}