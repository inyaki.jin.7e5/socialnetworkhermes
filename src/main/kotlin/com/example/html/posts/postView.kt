package com.example.html.posts

import kotlinx.html.FlowContent
import kotlinx.html.div
import kotlinx.html.p
import kotlinx.html.strong
import com.example.repository.tables.PostDao

fun FlowContent.postView(post : PostDao) {
    div() {
        p() {
            strong() {
                +post.users.joinToString { it.username }
            }
            + post.title
            + post.body
        }
    }
}