package com.example.html.posts

import io.ktor.server.application.*
import io.ktor.server.resources.*
import kotlinx.html.*
import com.example.repository.tables.PostDao
import com.example.routes.Posts

fun FlowContent.postsListView(application: Application, posts: List<PostDao>) {
    div() {
        h6() { +"""Recent updates""" }
        posts.forEach { post ->
            postView(post)
        }

        small() {
            a {
                href = application.href(Posts.New())
                +"""New Post"""
            }
        }
    }
}