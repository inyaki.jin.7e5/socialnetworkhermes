package com.example.html.posts

import kotlinx.html.*
import kotlinx.html.dom.create
import org.w3c.dom.events.Event
//import org.w3c.dom.HTMLFormElement
//import org.w3c.dom.HTMLInputElement
//import org.w3c.dom.asList
//import org.w3c.fetch.RequestInit
//import kotlin.browser.document
//import kotlin.js.json

fun FlowContent.postsFormView() {
    div {
        h4 { +"Create Message" }
        form(method = FormMethod.post) {
            div {
                label {
                    htmlFor = "title"
                    +"Title"
                }
                input {
                    type = InputType.text
                    name = "title"
                    id = "title"
                    placeholder = ""
                    value = ""
                }
                label {
                    htmlFor = "body"
                    +"Body"
                }
                input {
                    type = InputType.text
                    name = "body"
                    id = "body"
                    placeholder = ""
                    value = ""
                }
            }
            button {
                type = ButtonType.submit
                +"Create"
//                onClickFunction = onClickSubmit
            }
        }
    }
    script {
        unsafe { +"""
            const onClickSubmit = function(event) {
                event.preventDefault();
                var form = event.target.closest('form');
                var titleInput = form.querySelector('#title');
                var bodyInput = form.querySelector('#body');

                var title = titleInput.value;
                var body = bodyInput.value;

                fetch('/posts/new', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ 'title': title, 'body': body })
                })
                .then(response => response.json())
                .then(data => {
                    console.log('Insert successful:', data);
                    // Aquí puedes realizar cualquier acción adicional después de insertar los datos
                })
                .catch(error => {
                    console.error('Insert error:', error);
                    // Aquí puedes manejar cualquier error que ocurra durante la inserción
                });
            };
        """ }
    }
}

