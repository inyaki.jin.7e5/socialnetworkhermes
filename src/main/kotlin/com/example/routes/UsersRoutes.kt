package com.example.routes

import com.example.html.LayoutTemplate
import com.example.html.users.platformStatsView
import com.example.repository.UsersRepository
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.resources.*
import io.ktor.server.routing.*
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.transactions.transaction

@Serializable
@Resource("users")
class Users() {
}

fun Routing.usersRoutes() {
    val usersRepository = UsersRepository()
    get<Users> {
        val usersStats = usersRepository.userStats()
        call.respondHtmlTemplate(LayoutTemplate(application)) {
            content {
                transaction {
                    platformStatsView(usersStats)
                }
            }
        }
    }
}