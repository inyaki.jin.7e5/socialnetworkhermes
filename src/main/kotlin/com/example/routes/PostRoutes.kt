package com.example.routes

import com.example.html.LayoutTemplate
import com.example.html.posts.postsFormView
import com.example.html.posts.postsListView
import com.example.html.posts.administratorForm
import com.example.repository.PostsRepository
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.transactions.transaction
import io.ktor.server.resources.post
import com.example.models.Post
import com.example.models.UserSession
import io.ktor.http.*
import io.ktor.server.auth.*
import io.ktor.server.sessions.*
import kotlinx.html.Entities
import kotlinx.html.body
import kotlinx.html.p


@Serializable
@Resource("posts")
class Posts() {
    @Serializable
    @Resource("new")
    class New(val parent: Posts = Posts())
}

fun Routing.postsRoutes() {
    val postsRepository = PostsRepository()

    get("/posts") {
        try {
            val posts = postsRepository.list()
            call.respondHtmlTemplate(LayoutTemplate(call.application)) { // Accede a call.application para obtener la referencia de Application
                content {
                    transaction {
                        postsListView(call.application, posts) // Pasa call.application como argumento
                    }
                }
            }
        } catch (e: Exception) {
            println(e)
        }
    }

    get("/posts/new") {
        call.respondHtmlTemplate(LayoutTemplate(call.application)) {
            content {
                transaction {
                    postsFormView()
                }
            }
        }
    }

    post("/posts/new") { // Corrige la ruta para que coincida con la que estás utilizando en la redirección
        val parameters = call.receiveParameters()
        val title = parameters["title"]!!
        val body = parameters["body"]!!
        postsRepository.insert(title, body)
        val url: String = call.application.href(Posts) // Accede a call.application.href para obtener la URL
        call.respondRedirect(url)
    }
//    authenticate("auth-form") {
//        post("/login") {
//            val userName = call.principal<UserIdPrincipal>()?.name.toString()
//            call.sessions.set(UserSession(name = userName, count = 1))
//            call.respondRedirect("/home")
//        }
//    }
    authenticate("auth-form") {
        post("/login") {
            val userName = call.principal<UserIdPrincipal>()?.name.toString()
            call.sessions.set(UserSession(name = userName, count = 1))
            call.respondRedirect("/administrator") // Cambia la ruta de redirección a "/administrator"
        }
    }
    authenticate("auth-session") {
        get("/administrator") {
            val userSession = call.principal<UserSession>()
            call.sessions.set(UserSession.copy(count = userSession!!.count + 1))
            call.respondHtmlTemplate(LayoutTemplate(application)) {
                content {
                    transaction {
                        administratorForm()
                    }
                }
            }
        }
        get("/home") {
            val userSession = call.principal<UserSession>()
            call.sessions.set(UserSession.copy(count = userSession!!.count + 1))
            call.respondHtml {
                body {
                    p {
                        +"Hello, ${userSession?.name}! Visit count is ${userSession?.count}."
                    }
                }
            }
        }
    }
}