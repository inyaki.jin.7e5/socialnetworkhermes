package com.example.plugins

import com.example.repository.DatabaseManager
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*
import com.example.routes.postsRoutes
import com.example.routes.usersRoutes

fun Application.configureRouting() {
    routing {
        postsRoutes()
        usersRoutes()
        DatabaseManager()
    }
}
