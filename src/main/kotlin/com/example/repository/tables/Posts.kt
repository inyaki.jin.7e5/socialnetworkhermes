package com.example.repository.tables

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Posts : IntIdTable() {
    val title = text("title")
    val body = text("body")
}

class PostDao(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<PostDao>(Posts)
    var title by Posts.title
    var body by Posts.body
    var users by UserDao via UsersPosts
}
