package com.example.repository.tables

import org.jetbrains.exposed.sql.Table

object UsersPosts : Table() {
    val post = reference("post", Posts)
    val user = reference("user", Users)
    override val primaryKey = PrimaryKey(post, user)
}
