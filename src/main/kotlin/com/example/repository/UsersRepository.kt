package com.example.repository

import com.example.models.User
import com.example.models.UserStats
import com.example.repository.tables.Posts
import com.example.repository.tables.UsersPosts
import com.example.repository.tables.Users
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

class UsersRepository {
    fun userStats() :List<UserStats> = transaction {
        Users.join(UsersPosts, JoinType.LEFT, Users.id, UsersPosts.user).join(Posts, JoinType.LEFT, UsersPosts.user, Posts.id)
            .slice(Users.id, Users.username, Posts.id.count()).selectAll()
            .groupBy(Users.id, Users.username)
            .map(::mapToUserStats)
    }
}

fun mapToUserStats(resultRow: ResultRow): UserStats =
    UserStats(mapToUser(resultRow), resultRow[Posts.id.count()])


fun mapToUser(resultRow: ResultRow): User =
    User(resultRow[Users.username])

