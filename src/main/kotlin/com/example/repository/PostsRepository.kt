package com.example.repository

import com.example.repository.tables.PostDao
import org.jetbrains.exposed.sql.transactions.transaction

class PostsRepository {
    fun insert(title : String, body: String) = transaction{
        PostDao.new {
            this.title = title
            this.body = body
        }
    }

    fun list() = transaction {
        PostDao.all().toList()
    }
}
