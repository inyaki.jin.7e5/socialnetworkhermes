package com.example.repository

import com.example.repository.tables.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction



class DatabaseManager {
    init {
        // Database.connect("jdbc:postgresql://lucky.db.elephantsql.com/awrwsivx", password = "XXX", user = "awrwsivx", driver = "org.postgresql.Driver")
        Database.connect("jdbc:h2:./testdb.h2", driver = "org.h2.Driver")

        transaction {
            // print sql to std-out
            addLogger(StdOutSqlLogger)
            SchemaUtils.create(Posts, Users, UsersPosts)
            populateTestData()
        }
    }
    private fun populateTestData() {
            val post1 = PostDao.new { title = "post 1";
                body = "a"}
            val post2 = PostDao.new { title = "post 2";
            body = "a"}
            PostDao.new { title = "post 3";
                body = "a" }

            val platform1 = UserDao.new { username = "Inyaki"}
            val platform2 = UserDao.new { username = "Gerard" }

            post1.users = SizedCollection(listOf(platform1))
            post2.users = SizedCollection(listOf(platform1, platform2))
        }
    }
