package com.example.models

data class UserStats(val user: User, val postCount: Long) {
}